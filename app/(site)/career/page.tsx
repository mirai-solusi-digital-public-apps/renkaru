import React from "react";
import Career from "@/components/Career";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Career - Mirai Digital Solusi",
  description: "This is Career page for Mirai Digital Solusi",
  // other metadata
};

const SupportPage = () => {
  return (
    <div className="pb-20 pt-40">
      <Career />
    </div>
  );
};

export default SupportPage;
