import { FAQ } from "@/types/faq";

const faqData: FAQ[] = [
  {
    id: 1,
    quest: "How do you protect intellectual property and ensure confidentiality?",
    ans: "We take intellectual property and confidentiality very seriously. All our employees sign strict non-disclosure agreements (NDAs), and we have robust security measures in place to protect your data and intellectual property.",
  },
  {
    id: 2,
    quest: "How do you ensure on-time and on-budget project delivery?",
    ans: "We follow a rigorous project planning and estimation process, taking into account all project requirements, risks, and constraints. We also employ effective risk management strategies and maintain transparent communication with our clients throughout the project lifecycle. Our goal is to deliver projects on time and within the agreed-upon budget.",
  },
  // {
  //   id: 3,
  //   quest: "Do you follow any specific software development methodologies?",
  //   ans: "Yes, we are well-versed in various software development methodologies, including Agile (Scrum, Kanban), Waterfall, and Lean. We work closely with our clients to understand their requirements and determine the most suitable approach for their project.",
  // },
  // {
  //   id: 4,
  //   quest: "How do you ensure the quality of the software delivered?",
  //   ans: "Quality assurance is integral to our development process. We conduct comprehensive testing including unit testing, integration testing, system testing, and acceptance testing to ensure that the software meets the highest standards of quality and functionality.",
  // },
  {
    id: 3,
    quest: "Can you work with existing software systems or legacy codebases?",
    ans: "Absolutely, we have experience in working with existing software systems and legacy codebases. Whether it's integrating new features, refactoring code, or modernizing outdated systems, our team is adept at navigating complex environments to deliver the desired outcomes.",
  },
  {
    id: 4,
    quest: "Do you provide ongoing support and maintenance for the software?",
    ans: "Yes, we offer ongoing support and maintenance services to ensure your software operates smoothly post-deployment. Our support team is available to address any issues, implement updates, and provide technical assistance as needed.",
  },
  {
    id: 5,
    quest: "How do I get started with your services?",
    ans: "Getting started is simple! Reach out to us via our contact form, email, or phone to discuss your project requirements. Our team will schedule a consultation to understand your needs, provide recommendations, and outline a tailored approach to address your software development needs.",
  },
];

export default faqData;
