import { Brand } from "@/types/brand";

const brandData: Brand[] = [
  {
    id: 0.25,
    name: "Client",
    href: "#",
    image: "/images/brand/tuku.svg",
    imageLight: "/images/brand/tuku.png",
  },
  {
    id: 0.3,
    name: "Client",
    href: "#",
    image: "/images/brand/sdn.png",
    imageLight: "/images/brand/sdn.png",
  },
  {
    id: 0.4,
    name: "Client",
    href: "#",
    image: "/images/brand/nadaku.png",
    imageLight: "/images/brand/nadaku.png",
  },
  {
    id: 0.5,
    name: "Client",
    href: "#",
    image: "/images/brand/codr.jpeg",
    imageLight: "/images/brand/codr.jpeg",
  },
  {
    id: 0.6,
    name: "Client",
    href: "#",
    image: "/images/brand/asaba.webp",
    imageLight: "/images/brand/asaba.webp",
  },
  {
    id: 0.7,
    name: "Client",
    href: "#",
    image: "/images/brand/zesthub.png",
    imageLight: "/images/brand/zesthub.png",
  },
];

export default brandData;
