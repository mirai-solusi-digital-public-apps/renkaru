import { Feature } from "@/types/feature";

const featuresData: Feature[] = [
  {
    id: 1,
    icon: "/images/icon/icon-01.svg",
    title: "Custom Web Apps",
    description:
      "We have services to develop custom web applications to meet your enterprise's needs.",
  },
  {
    id: 2,
    icon: "/images/icon/icon-02.svg",
    title: "Data Warehouse",
    description:
      "Data is the new oil. So we are committed to providing data warehouse development services so that companies can make the best use of data.",
  },
  {
    id: 3,
    icon: "/images/icon/icon-03.svg",
    title: "Automation Apps",
    description:
      "If you want efficiency in your business processes, we can help you develop automation applications to complete repetitive tasks quickly and create artificial intelligence to make decisions.",
  },
];

export default featuresData;
