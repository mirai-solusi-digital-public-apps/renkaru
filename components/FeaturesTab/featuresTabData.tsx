import { FeatureTab } from "@/types/featureTab";

const featuresTabData: FeatureTab[] = [
  {
    id: "tabOne",
    title: "No Need to Hesitate - Ask Away",
    desc1: `At our company, we believe in providing personalized solutions tailored to each client's unique needs. Through our free consultation service, we take the time to actively listen and understand your specific requirements and challenges.`,
    desc2: `    Our team of experts will then conduct an in-depth analysis to gain a comprehensive understanding of your business's pain points.`,
    image: "/images/features/features-light-consult-01.png",
    imageDark: "/images/features/features-dark-consult-01.svg",
  },
  {
    id: "tabTwo",
    title: "Unwavering Support - 4+ Months of Free Assistance",
    desc1: `We understand that even after an application is successfully delivered, unforeseen challenges or issues may arise during its implementation or usage. That's why we prioritize long-term support for our clients. Our commitment to exceptional service doesn't end with the delivery of the application. `,
    desc2: `    Instead, we provide comprehensive, free support for a minimum of four months to ensure a seamless transition and address any concerns or questions that may emerge.Our dedicated team is readily available to guide you through the process, offer troubleshooting assistance, and ensure that the application continues to meet your evolving needs.`,
    image: "/images/features/features-light-support-01.png",
    imageDark: "/images/features/features-dark-support-01.svg",
  },
  {
    id: "tabThree",
    title: "Guaranteed Quality - Rigorous Testing for Flawless Applications",
    desc1: ` At our core, we are committed to delivering exceptional quality in every application we create for our clients. This commitment is backed by a robust quality assurance process that includes rigorous testing and validation. Our dedicated team of experts meticulously evaluates each aspect of the application, ensuring it meets the highest standards of functionality, performance, and reliability.`,
    desc2: `With our unwavering focus on quality assurance, you can trust that the applications we develop will not only meet but exceed your expectations, providing a seamless and efficient solution to drive your business forward.`,
    image: "/images/features/features-light-quality-01.png",
    imageDark: "/images/features/features-dark-quality-01.svg",
  },
];

export default featuresTabData;
