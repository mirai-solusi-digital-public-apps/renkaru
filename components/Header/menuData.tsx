import { Menu } from "@/types/menu";

const menuData: Menu[] = [
  {
    id: 1,
    title: "Home",
    newTab: false,
    path: "/",
  },
  {
    id: 2,
    title: "Fleet",
    newTab: false,
    path: "/fleet",
  },
  {
    id: 3,
    title: "About Us",
    newTab: false,
    path: "/about",
  },
  {
    id: 4,
    title: "Terms",
    newTab: false,
    path: "/support",
  },
];

export default menuData;
